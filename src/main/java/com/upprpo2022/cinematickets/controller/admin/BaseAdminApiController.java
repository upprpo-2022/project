package com.upprpo2022.cinematickets.controller.admin;

import com.upprpo2022.cinematickets.dto.admin.PageDto;
import com.upprpo2022.cinematickets.dto.admin.mappers.AdminApiDtoMapper;
import com.upprpo2022.cinematickets.exception.ApiResponseStatusException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import static com.upprpo2022.cinematickets.exception.ErrorString.BAD_REQUEST;
import static com.upprpo2022.cinematickets.exception.ErrorString.NO_SUCH_ENTITY;

// E - entity
// D - DTO corresponding to the entity
public abstract class BaseAdminApiController<E, D> {
    private final JpaRepository<E, Integer> repository;
    private final AdminApiDtoMapper<E, D> adminApiDtoMapper;

    protected BaseAdminApiController(JpaRepository<E, Integer> repository, AdminApiDtoMapper<E, D> adminApiDtoMapper) {
        this.repository = repository;
        this.adminApiDtoMapper = adminApiDtoMapper;
    }

    @GetMapping
    public PageDto<D> getEntities(@RequestParam("page") Integer pageNum) {
        if (pageNum < 1) {
            throw new ApiResponseStatusException(HttpStatus.BAD_REQUEST, BAD_REQUEST, "Bad page number");
        }

        Page<E> page = repository.findAll(PageRequest.of(pageNum - 1, 50).withSort(Sort.by("id").ascending()));

        PageDto<D> pageDto = new PageDto<>();
        pageDto.setPageNum(pageNum);
        pageDto.setMaxPages(page.getTotalPages());
        pageDto.setList(page.map(adminApiDtoMapper::entityToDto).toList());

        return pageDto;
    }

    @GetMapping("/{id}")
    public D getEntity(@PathVariable("id") Integer id) {
        E entity = repository.findById(id).orElseThrow(() -> new ApiResponseStatusException(HttpStatus.BAD_REQUEST, NO_SUCH_ENTITY, "Entity with id does not exist"));
        return adminApiDtoMapper.entityToDto(entity);
    }

    @PostMapping
    public void createEntity(@RequestBody D dto) {
        E entity = adminApiDtoMapper.dtoToEntity(dto);
        repository.save(entity);
    }

    @DeleteMapping("/{id}")
    public void deleteEntity(@PathVariable("id") Integer id) {
        E entity = repository.findById(id).orElseThrow(() -> new ApiResponseStatusException(HttpStatus.BAD_REQUEST, NO_SUCH_ENTITY, "Entity with id does not exist"));
        repository.delete(entity);
    }
}
