package com.upprpo2022.cinematickets.controller.admin;

import com.upprpo2022.cinematickets.service.AdminMoviePreviewS3Service;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
@RequestMapping("/api/v1/admin")
public class AdminMoviePreviewS3Controller {
    private final AdminMoviePreviewS3Service adminMoviePreviewS3Service;

    public AdminMoviePreviewS3Controller(AdminMoviePreviewS3Service adminMoviePreviewS3Service) {
        this.adminMoviePreviewS3Service = adminMoviePreviewS3Service;
    }

    @PostMapping(value = "upload-movie-preview")
    public void uploadMoviePreview(@RequestParam("id") Integer movieId, @RequestParam("file") MultipartFile file) throws IOException {
        adminMoviePreviewS3Service.uploadMoviePreview(movieId, file);
    }
}
