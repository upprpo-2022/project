package com.upprpo2022.cinematickets.controller.admin;

import com.upprpo2022.cinematickets.domain.Auditorium;
import com.upprpo2022.cinematickets.dto.admin.AdminAuditoriumDto;
import com.upprpo2022.cinematickets.dto.admin.mappers.AdminApiDtoMapper;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/admin/auditoriums")
public class AuditoriumAdminApiController extends BaseAdminApiController<Auditorium, AdminAuditoriumDto> {
    protected AuditoriumAdminApiController(JpaRepository<Auditorium, Integer> repository, AdminApiDtoMapper<Auditorium, AdminAuditoriumDto> adminApiDtoMapper) {
        super(repository, adminApiDtoMapper);
    }
}
