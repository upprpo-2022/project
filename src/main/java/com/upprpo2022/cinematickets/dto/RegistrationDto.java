package com.upprpo2022.cinematickets.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class RegistrationDto {
    @JsonProperty("username") private String username;
    @JsonProperty("password") private String password;
}
