package com.upprpo2022.cinematickets.dto.admin;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.upprpo2022.cinematickets.domain.SeatLayout;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@ToString
public class AdminAuditoriumDto {
    @JsonProperty("id") private Integer id;
    @JsonProperty("theaterId") private Integer theaterId;
    @JsonProperty("seats") private List<SeatLayout.Seat> seats;
}
