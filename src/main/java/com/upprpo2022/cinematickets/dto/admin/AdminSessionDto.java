package com.upprpo2022.cinematickets.dto.admin;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.upprpo2022.cinematickets.domain.SeatLayoutStatus;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.time.OffsetDateTime;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@ToString
public class AdminSessionDto {
    @JsonProperty("id") private Integer id;
    @JsonProperty("theaterId") private Integer theaterId;
    @JsonProperty("movieId") private Integer movieId;
    @JsonProperty("auditoriumId") private Integer auditoriumId;
    @JsonProperty("seats") private List<SeatLayoutStatus.Seat> seats;
    @JsonProperty("dateTime") private OffsetDateTime dateTime;
}
