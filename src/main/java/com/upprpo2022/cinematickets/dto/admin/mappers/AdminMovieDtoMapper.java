package com.upprpo2022.cinematickets.dto.admin.mappers;

import com.upprpo2022.cinematickets.domain.Movie;
import com.upprpo2022.cinematickets.dto.admin.AdminMovieDto;
import org.mapstruct.Mapper;

@Mapper
public interface AdminMovieDtoMapper extends AdminApiDtoMapper<Movie, AdminMovieDto> {
    AdminMovieDto entityToDto(Movie entity);
    Movie dtoToEntity(AdminMovieDto dto);
}
