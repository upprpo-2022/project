package com.upprpo2022.cinematickets.dto.admin.mappers;

import com.upprpo2022.cinematickets.domain.AppUser;
import com.upprpo2022.cinematickets.dto.admin.AdminUserDto;
import org.mapstruct.Mapper;

@Mapper
public interface AdminUserDtoMapper extends AdminApiDtoMapper<AppUser, AdminUserDto> {
    AdminUserDto entityToDto(AppUser entity);
    AppUser dtoToEntity(AdminUserDto dto);
}
