package com.upprpo2022.cinematickets.dto.admin;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@ToString
public class PageDto <T> {
    @JsonProperty("maxPages") private int maxPages;
    @JsonProperty("pageNum") private int pageNum;
    @JsonProperty("list") private List<T> list;
}
