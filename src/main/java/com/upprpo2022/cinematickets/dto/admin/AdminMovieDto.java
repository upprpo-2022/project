package com.upprpo2022.cinematickets.dto.admin;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@ToString
public class AdminMovieDto {
    @JsonProperty("id") private Integer id;
    @JsonProperty("name") private String name;
    @JsonProperty("description") private String description;
    @JsonProperty("previewUrl") private String previewUrl;
}
