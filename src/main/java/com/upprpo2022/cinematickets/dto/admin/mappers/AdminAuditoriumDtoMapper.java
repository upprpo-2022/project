package com.upprpo2022.cinematickets.dto.admin.mappers;

import com.upprpo2022.cinematickets.domain.Auditorium;
import com.upprpo2022.cinematickets.dto.admin.AdminAuditoriumDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper
public interface AdminAuditoriumDtoMapper extends AdminApiDtoMapper<Auditorium, AdminAuditoriumDto> {
    @Mapping(target = "seats", source = "seats.seats")
    AdminAuditoriumDto entityToDto(Auditorium entity);

    @Mapping(target = "seats.seats", source = "seats")
    Auditorium dtoToEntity(AdminAuditoriumDto dto);
}
