package com.upprpo2022.cinematickets.dto.admin.mappers;

public interface AdminApiDtoMapper<E, D> {
    D entityToDto(E entity);
    E dtoToEntity(D dto);
}
