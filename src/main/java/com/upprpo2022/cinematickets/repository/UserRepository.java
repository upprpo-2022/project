package com.upprpo2022.cinematickets.repository;

import com.upprpo2022.cinematickets.domain.AppUser;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<AppUser, Integer> {
    AppUser findByUsername(String username);
}
