package com.upprpo2022.cinematickets.repository;

import com.upprpo2022.cinematickets.domain.Theater;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TheaterRepository extends JpaRepository<Theater, Integer> {

}
