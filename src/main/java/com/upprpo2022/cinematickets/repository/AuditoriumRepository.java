package com.upprpo2022.cinematickets.repository;

import com.upprpo2022.cinematickets.domain.Auditorium;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AuditoriumRepository extends JpaRepository<Auditorium, Integer> {

}
