package com.upprpo2022.cinematickets.domain;

import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import javax.persistence.*;
import java.time.OffsetDateTime;

@Getter
@Setter
@ToString
@Entity(name = "Session")
@Table(name = "session")
@TypeDefs(@TypeDef(name = "jsonb", typeClass = JsonBinaryType.class))
public class Session {
    @Id
    @Column(name = "id", columnDefinition = "SERIAL", unique = true, nullable = false)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "movie_id", referencedColumnName = "id", columnDefinition = "INTEGER", insertable = false, updatable = false, nullable = false)
    private Movie movie;

    @Column(name = "movie_id", nullable = false)
    private Integer movieId;

    @ManyToOne
    @JoinColumn(name = "theater_id", referencedColumnName = "id", columnDefinition = "INTEGER", insertable = false, updatable = false, nullable = false)
    private Theater theater;

    @Column(name = "theater_id", nullable = false)
    private Integer theaterId;

    @ManyToOne
    @JoinColumn(name = "auditorium_id", referencedColumnName = "id", columnDefinition = "INTEGER", insertable = false, updatable = false, nullable = false)
    private Auditorium auditorium;

    @Column(name = "auditorium_id", nullable = false)
    private Integer auditoriumId;

    @Column(name = "seats", columnDefinition = "jsonb", nullable = false)
    @Type(type = "jsonb")
    private SeatLayoutStatus seats;

    @Column(name = "datetime", columnDefinition = "timestamp with time zone", nullable = false)
    private OffsetDateTime dateTime;
}
