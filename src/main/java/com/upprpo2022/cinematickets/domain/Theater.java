package com.upprpo2022.cinematickets.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Getter
@Setter
@ToString
@Entity(name = "Theater")
@Table(name = "theater")
public class Theater {
    @Id
    @Column(name = "id", columnDefinition = "SERIAL", unique = true, nullable = false)
    private Integer id;

    @Column(name = "name", columnDefinition = "VARCHAR(50)", nullable = false)
    private String name;

    @Column(name = "address", columnDefinition = "VARCHAR(100)", nullable = false)
    private String address;
}
