package com.upprpo2022.cinematickets.domain;

import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import javax.persistence.*;

@Getter
@Setter
@ToString
@Entity(name = "Auditorium")
@Table(name = "auditorium")
@TypeDefs(@TypeDef(name = "jsonb", typeClass = JsonBinaryType.class))
public class Auditorium {
    @Id
    @Column(name = "id", columnDefinition = "SERIAL", unique = true, nullable = false)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "theater_id", referencedColumnName = "id", columnDefinition = "INTEGER", insertable = false, updatable = false, nullable = false)
    private Theater theater;

    @Column(name = "theater_id", nullable = false)
    private Integer theaterId;

    @Column(name = "seats", columnDefinition = "jsonb", nullable = false)
    @Type(type = "jsonb")
    private SeatLayout seats;
}
