package com.upprpo2022.cinematickets.service;

import com.upprpo2022.cinematickets.domain.SeatLayoutStatus;
import com.upprpo2022.cinematickets.domain.Session;
import com.upprpo2022.cinematickets.dto.TicketOrderDto;
import com.upprpo2022.cinematickets.exception.ApiResponseStatusException;
import com.upprpo2022.cinematickets.repository.SessionRepository;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.upprpo2022.cinematickets.exception.ErrorString.BAD_REQUEST;

@Service
public class TicketOrderService {
    private final SessionRepository sessionRepository;

    public TicketOrderService(SessionRepository sessionRepository) {
        this.sessionRepository = sessionRepository;
    }

    public void handleTicketOrder(TicketOrderDto ticketOrderDto) {
        int sessionId = ticketOrderDto.getSessionId();
        List<TicketOrderDto.Seat> seatsDto = ticketOrderDto.getSeats();

        Session session = sessionRepository.findById(sessionId)
                .orElseThrow(() -> new ApiResponseStatusException(HttpStatus.BAD_REQUEST, BAD_REQUEST, "Session id was not found"));

        seatsDto.forEach(seatDto -> {
                    SeatLayoutStatus.Seat seatLayoutStatusToEdit = session.getSeats().getSeats().stream()
                            .filter(seat -> seatDto.getX().equals(seat.getX()) && seatDto.getY().equals(seat.getY()))
                            .findFirst()
                            .orElseThrow(() -> new ApiResponseStatusException(HttpStatus.BAD_REQUEST, BAD_REQUEST, "Bad seat coordinates"));

                    switch (seatLayoutStatusToEdit.getStatus()) {
                        case OCCUPIED -> throw new ApiResponseStatusException(HttpStatus.BAD_REQUEST, BAD_REQUEST, "Some seats are already occupied");
                        case FREE -> seatLayoutStatusToEdit.setStatus(SeatLayoutStatus.Seat.Status.OCCUPIED);
                    }
                }
        );

        sessionRepository.save(session);
    }
}
