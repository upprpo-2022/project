package com.upprpo2022.cinematickets.service;

import com.upprpo2022.cinematickets.dto.MovieScheduleDto;
import com.upprpo2022.cinematickets.projections.MovieScheduleProjection;
import com.upprpo2022.cinematickets.repository.SessionRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class SessionService {
    private final SessionRepository sessionRepository;

    public SessionService(SessionRepository sessionRepository) {
        this.sessionRepository = sessionRepository;
    }

    public MovieScheduleDto getMovieSchedule(int movieId, OffsetDateTime startDate, OffsetDateTime endDate, int pageNum) {
        Page<MovieScheduleProjection> page = sessionRepository.findMovieScheduleByMovieIdAndDateTimeBetween(movieId, startDate, endDate,
                PageRequest.of(pageNum - 1, 50).withSort(Sort.by("theater.id").ascending()));

        Map<Integer, List<MovieScheduleProjection>> theaterMap = page.stream()
                .collect(Collectors.groupingBy(MovieScheduleProjection::getTheaterId, Collectors.toList()));

        List<MovieScheduleDto.TheaterDto> theaterDtos = theaterMap.values().stream()
                .map(projList -> new MovieScheduleDto.TheaterDto(
                                projList.get(0).getId(),
                                projList.get(0).getTheaterName(),
                                projList.get(0).getTheaterAddress(),
                                projList.stream().map(proj -> new MovieScheduleDto.TheaterDto.SessionDto(
                                        proj.getId(),
                                        proj.getDateTime())
                                ).toList()
                        )
                ).toList();

        return new MovieScheduleDto(page.getTotalPages(), pageNum, theaterDtos);
    }
}
