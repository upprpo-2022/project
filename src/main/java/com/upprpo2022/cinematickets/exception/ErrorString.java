package com.upprpo2022.cinematickets.exception;

public enum ErrorString {
    UNEXPECTED_ERROR("UNEXPECTED_ERROR"),
    BAD_CREDENTIALS("BAD_CREDENTIALS"),
    BAD_REQUEST("BAD_REQUEST"),
    USERNAME_TAKEN("USERNAME_TAKEN"),
    BAD_USERNAME("BAD_USERNAME"),
    BAD_PASSWORD("BAD_PASSWORD"),
    NO_SUCH_ENTITY("NO_SUCH_ENTITY");

    private final String stringValue;

    ErrorString(String stringValue) {
        this.stringValue = stringValue;
    }

    @Override
    public String toString() {
        return stringValue;
    }
}
