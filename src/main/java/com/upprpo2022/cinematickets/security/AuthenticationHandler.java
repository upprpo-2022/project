package com.upprpo2022.cinematickets.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.upprpo2022.cinematickets.dto.ApiErrorDto;
import com.upprpo2022.cinematickets.dto.LoginRoleDto;
import com.upprpo2022.cinematickets.exception.ApiResponseStatusException;
import com.upprpo2022.cinematickets.exception.ExceptionHandlerAdvice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.upprpo2022.cinematickets.exception.ErrorString.BAD_CREDENTIALS;
import static com.upprpo2022.cinematickets.exception.ErrorString.UNEXPECTED_ERROR;

// Using ObjectMapper and ExceptionHandlerAdvice directly because ViewResolver is not being used by Spring here
public class AuthenticationHandler implements AuthenticationSuccessHandler, AuthenticationFailureHandler {
    private static final ObjectMapper objectMapper = new ObjectMapper();
    @Autowired
    private ExceptionHandlerAdvice exceptionHandlerAdvice;

    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) throws IOException, ServletException {
        ResponseEntity<ApiErrorDto> responseEntity;

        if (exception instanceof BadCredentialsException) {
            responseEntity = exceptionHandlerAdvice.apiResponseStatusExceptionHandler(new ApiResponseStatusException(HttpStatus.UNAUTHORIZED, BAD_CREDENTIALS));
            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        } else {
            responseEntity = exceptionHandlerAdvice.apiResponseStatusExceptionHandler(new ApiResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, UNEXPECTED_ERROR));
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }

        response.getWriter().write(objectMapper.writeValueAsString(responseEntity.getBody()));
    }

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        response.setStatus(HttpServletResponse.SC_OK);
        response.setContentType(MediaType.APPLICATION_JSON_VALUE);

        LoginRoleDto loginRoleDto = new LoginRoleDto();
        String role = ((GrantedAuthority) authentication.getAuthorities().toArray()[0]).getAuthority().substring(5);
        loginRoleDto.setRole(role);

        response.getWriter().print(objectMapper.writeValueAsString(loginRoleDto));
    }
}
