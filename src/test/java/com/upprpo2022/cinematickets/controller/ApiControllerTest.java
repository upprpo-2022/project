package com.upprpo2022.cinematickets.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.upprpo2022.cinematickets.domain.*;
import com.upprpo2022.cinematickets.dto.TicketOrderDto;
import com.upprpo2022.cinematickets.repository.MovieRepository;
import com.upprpo2022.cinematickets.repository.SessionRepository;
import com.upprpo2022.cinematickets.service.TicketOrderService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.Optional;

import static org.mockito.Mockito.*;

@ContextConfiguration(classes = {ApiController.class})
@ExtendWith(SpringExtension.class)
class ApiControllerTest {
    @Autowired
    private ApiController apiController;

    @MockBean
    private MovieRepository movieRepository;

    @MockBean
    private SessionRepository sessionRepository;

    @MockBean
    private TicketOrderService ticketOrderService;

    @Test
    void testGetMovie() throws Exception {
        Movie movie = new Movie();
        movie.setDescription("The characteristics of someone or something");
        movie.setId(1);
        movie.setName("Name");
        movie.setPreviewObjectKey("Preview Object Key");
        movie.setPreviewUrl("https://example.org/example");
        Optional<Movie> ofResult = Optional.of(movie);
        when(this.movieRepository.findById(any())).thenReturn(ofResult);
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/api/v1/movie/{id}", 1);
        MockMvcBuilders.standaloneSetup(this.apiController)
                .build()
                .perform(requestBuilder)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType("application/json"))
                .andExpect(MockMvcResultMatchers.content()
                        .string(
                                "{\"id\":1,\"name\":\"Name\",\"description\":\"The characteristics of someone or something\",\"previewUrl\":\"https"
                                        + "://example.org/example\"}"));
    }

    @Test
    void testGetMovie2() throws Exception {
        Movie movie = new Movie();
        movie.setDescription("The characteristics of someone or something");
        movie.setId(1);
        movie.setName("Name");
        movie.setPreviewObjectKey("Preview Object Key");
        movie.setPreviewUrl("https://example.org/example");
        Optional<Movie> ofResult = Optional.of(movie);
        when(this.movieRepository.findById(any())).thenReturn(ofResult);
        MockHttpServletRequestBuilder getResult = MockMvcRequestBuilders.get("/api/v1/movie/{id}", 1);
        getResult.contentType("https://example.org/example");
        MockMvcBuilders.standaloneSetup(this.apiController)
                .build()
                .perform(getResult)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType("application/json"))
                .andExpect(MockMvcResultMatchers.content()
                        .string(
                                "{\"id\":1,\"name\":\"Name\",\"description\":\"The characteristics of someone or something\",\"previewUrl\":\"https"
                                        + "://example.org/example\"}"));
    }

    @Test
    void testGetSessionSeats() throws Exception {
        SeatLayout seatLayout = new SeatLayout();
        seatLayout.setSeats(new ArrayList<>());

        Theater theater = new Theater();
        theater.setAddress("42 Main St");
        theater.setId(1);
        theater.setName("Name");

        Auditorium auditorium = new Auditorium();
        auditorium.setId(1);
        auditorium.setSeats(seatLayout);
        auditorium.setTheater(theater);
        auditorium.setTheaterId(123);

        Movie movie = new Movie();
        movie.setDescription("The characteristics of someone or something");
        movie.setId(1);
        movie.setName("Name");
        movie.setPreviewObjectKey("Preview Object Key");
        movie.setPreviewUrl("https://example.org/example");

        SeatLayoutStatus seatLayoutStatus = new SeatLayoutStatus();
        seatLayoutStatus.setSeats(new ArrayList<>());

        Theater theater1 = new Theater();
        theater1.setAddress("42 Main St");
        theater1.setId(1);
        theater1.setName("Name");

        Session session = new Session();
        session.setAuditorium(auditorium);
        session.setAuditoriumId(123);
        session.setDateTime(null);
        session.setId(1);
        session.setMovie(movie);
        session.setMovieId(123);
        session.setSeats(seatLayoutStatus);
        session.setTheater(theater1);
        session.setTheaterId(123);
        Optional<Session> ofResult = Optional.of(session);
        when(this.sessionRepository.findById(any())).thenReturn(ofResult);
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/api/v1/sessions/{id}/seats", 1);
        MockMvcBuilders.standaloneSetup(this.apiController)
                .build()
                .perform(requestBuilder)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType("application/json"))
                .andExpect(MockMvcResultMatchers.content().string("{\"seats\":[]}"));
    }

    @Test
    void testGetSessionSeats2() throws Exception {
        SeatLayout seatLayout = new SeatLayout();
        seatLayout.setSeats(new ArrayList<>());

        Theater theater = new Theater();
        theater.setAddress("42 Main St");
        theater.setId(1);
        theater.setName("Name");

        Auditorium auditorium = new Auditorium();
        auditorium.setId(1);
        auditorium.setSeats(seatLayout);
        auditorium.setTheater(theater);
        auditorium.setTheaterId(123);

        Movie movie = new Movie();
        movie.setDescription("The characteristics of someone or something");
        movie.setId(1);
        movie.setName("Name");
        movie.setPreviewObjectKey("Preview Object Key");
        movie.setPreviewUrl("https://example.org/example");

        SeatLayoutStatus seatLayoutStatus = new SeatLayoutStatus();
        seatLayoutStatus.setSeats(new ArrayList<>());

        Theater theater1 = new Theater();
        theater1.setAddress("42 Main St");
        theater1.setId(1);
        theater1.setName("Name");

        Session session = new Session();
        session.setAuditorium(auditorium);
        session.setAuditoriumId(123);
        session.setDateTime(null);
        session.setId(1);
        session.setMovie(movie);
        session.setMovieId(123);
        session.setSeats(seatLayoutStatus);
        session.setTheater(theater1);
        session.setTheaterId(123);
        Optional<Session> ofResult = Optional.of(session);
        when(this.sessionRepository.findById(any())).thenReturn(ofResult);
        MockHttpServletRequestBuilder getResult = MockMvcRequestBuilders.get("/api/v1/sessions/{id}/seats", 1);
        getResult.contentType("https://example.org/example");
        MockMvcBuilders.standaloneSetup(this.apiController)
                .build()
                .perform(getResult)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType("application/json"))
                .andExpect(MockMvcResultMatchers.content().string("{\"seats\":[]}"));
    }

    @Test
    void testGetSessionsSchedule() throws Exception {
        MockHttpServletRequestBuilder getResult = MockMvcRequestBuilders.get("/api/v1/movies");
        MockHttpServletRequestBuilder requestBuilder = getResult.param("pageNum", String.valueOf(1));
        ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.apiController)
                .build()
                .perform(requestBuilder);
        actualPerformResult.andExpect(MockMvcResultMatchers.status().is(400));
    }

    @Test
    void testGetSessionsSchedule2() throws Exception {
        MockHttpServletRequestBuilder getResult = MockMvcRequestBuilders.get("/api/v1/movie-schedule");
        MockHttpServletRequestBuilder paramResult = getResult.param("endDate", String.valueOf((Object) null));
        MockHttpServletRequestBuilder paramResult1 = paramResult.param("movieId", String.valueOf(1));
        MockHttpServletRequestBuilder paramResult2 = paramResult1.param("pageNum", String.valueOf(1));
        MockHttpServletRequestBuilder requestBuilder = paramResult2.param("startDate", String.valueOf((Object) null));
        ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.apiController)
                .build()
                .perform(requestBuilder);
        actualPerformResult.andExpect(MockMvcResultMatchers.status().is(400));
    }

    @Test
    void testPostTicketOrder() throws Exception {
        doNothing().when(this.ticketOrderService).handleTicketOrder(any());

        TicketOrderDto ticketOrderDto = new TicketOrderDto();
        ticketOrderDto.setSeats(new ArrayList<>());
        ticketOrderDto.setSessionId(123);
        String content = (new ObjectMapper()).writeValueAsString(ticketOrderDto);
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post("/api/v1/ticket-order")
                .contentType(MediaType.APPLICATION_JSON)
                .content(content);
        MockMvcBuilders.standaloneSetup(this.apiController)
                .build()
                .perform(requestBuilder)
                .andExpect(MockMvcResultMatchers.status().isOk());
    }
}

