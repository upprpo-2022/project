package com.upprpo2022.cinematickets.controller;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.doNothing;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.upprpo2022.cinematickets.dto.ChangePasswordDto;
import com.upprpo2022.cinematickets.dto.RegistrationDto;
import com.upprpo2022.cinematickets.security.AppUserDetailsService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@ContextConfiguration(classes = {AuthController.class})
@ExtendWith(SpringExtension.class)
class AuthControllerTest {
    @MockBean
    private AppUserDetailsService appUserDetailsService;

    @Autowired
    private AuthController authController;

    @Test
    void testChangePassword() throws Exception {
        doNothing().when(this.appUserDetailsService).changeAppUserPassword(any());

        ChangePasswordDto changePasswordDto = new ChangePasswordDto();
        changePasswordDto.setNewPassword("iloveyou");
        changePasswordDto.setOldPassword("iloveyou");
        changePasswordDto.setUsername("janedoe");
        String content = (new ObjectMapper()).writeValueAsString(changePasswordDto);
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post("/change-password")
                .contentType(MediaType.APPLICATION_JSON)
                .content(content);
        MockMvcBuilders.standaloneSetup(this.authController)
                .build()
                .perform(requestBuilder)
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    void testRegister() throws Exception {
        doNothing().when(this.appUserDetailsService).registerAppUser(any());

        RegistrationDto registrationDto = new RegistrationDto();
        registrationDto.setPassword("iloveyou");
        registrationDto.setUsername("janedoe");
        String content = (new ObjectMapper()).writeValueAsString(registrationDto);
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post("/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content(content);
        MockMvcBuilders.standaloneSetup(this.authController)
                .build()
                .perform(requestBuilder)
                .andExpect(MockMvcResultMatchers.status().isOk());
    }
}

