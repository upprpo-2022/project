package com.upprpo2022.cinematickets.controller.admin;

import com.upprpo2022.cinematickets.domain.Movie;
import com.upprpo2022.cinematickets.repository.MovieRepository;
import com.upprpo2022.cinematickets.service.AdminMoviePreviewS3Service;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.mock.web.MockMultipartFile;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Optional;

import static org.mockito.Mockito.*;

class AdminMoviePreviewS3ControllerTest {
    @Test
    @Disabled("TODO: Complete this test")
    void testUploadMoviePreview() throws IOException {

        Movie movie = new Movie();
        movie.setDescription("The characteristics of someone or something");
        movie.setId(1);
        movie.setName("Name");
        movie.setPreviewObjectKey("Preview Object Key");
        movie.setPreviewUrl("https://example.org/example");
        MovieRepository movieRepository = mock(MovieRepository.class);
        when(movieRepository.findById(any())).thenReturn(Optional.of(movie));
        AdminMoviePreviewS3Controller adminMoviePreviewS3Controller = new AdminMoviePreviewS3Controller(
                new AdminMoviePreviewS3Service("s3://bucket-name/object-key", null, movieRepository));
        adminMoviePreviewS3Controller.uploadMoviePreview(1, new MockMultipartFile("Name", "AAAAAAAA".getBytes(StandardCharsets.UTF_8)));
    }

    @Test
    @Disabled("TODO: Complete this test")
    void testUploadMoviePreview2() throws IOException {
        Movie movie = mock(Movie.class);
        when(movie.getPreviewObjectKey()).thenReturn("Preview Object Key");
        doNothing().when(movie).setDescription(any());
        doNothing().when(movie).setId(any());
        doNothing().when(movie).setName(any());
        doNothing().when(movie).setPreviewObjectKey(any());
        doNothing().when(movie).setPreviewUrl(any());
        movie.setDescription("The characteristics of someone or something");
        movie.setId(1);
        movie.setName("Name");
        movie.setPreviewObjectKey("Preview Object Key");
        movie.setPreviewUrl("https://example.org/example");
        MovieRepository movieRepository = mock(MovieRepository.class);
        when(movieRepository.findById(any())).thenReturn(Optional.of(movie));
        AdminMoviePreviewS3Controller adminMoviePreviewS3Controller = new AdminMoviePreviewS3Controller(
                new AdminMoviePreviewS3Service("s3://bucket-name/object-key", null, movieRepository));
        adminMoviePreviewS3Controller.uploadMoviePreview(1, new MockMultipartFile("Name", "AAAAAAAA".getBytes(StandardCharsets.UTF_8)));
    }

    @Test
    @Disabled("TODO: Complete this test")
    void testUploadMoviePreview3() throws IOException {
        Movie movie = mock(Movie.class);
        when(movie.getPreviewObjectKey()).thenReturn("Preview Object Key");
        doNothing().when(movie).setDescription(any());
        doNothing().when(movie).setId(any());
        doNothing().when(movie).setName(any());
        doNothing().when(movie).setPreviewObjectKey(any());
        doNothing().when(movie).setPreviewUrl(any());
        movie.setDescription("The characteristics of someone or something");
        movie.setId(1);
        movie.setName("Name");
        movie.setPreviewObjectKey("Preview Object Key");
        movie.setPreviewUrl("https://example.org/example");
        MovieRepository movieRepository = mock(MovieRepository.class);
        when(movieRepository.findById(any())).thenReturn(Optional.empty());
        AdminMoviePreviewS3Controller adminMoviePreviewS3Controller = new AdminMoviePreviewS3Controller(
                new AdminMoviePreviewS3Service("s3://bucket-name/object-key", null, movieRepository));
        adminMoviePreviewS3Controller.uploadMoviePreview(1, new MockMultipartFile("Name", "AAAAAAAA".getBytes(StandardCharsets.UTF_8)));
    }

    @Test
    void testUploadMoviePreview4() throws IOException {
        Movie movie = mock(Movie.class);
        when(movie.getPreviewObjectKey()).thenReturn("Preview Object Key");
        doNothing().when(movie).setDescription(any());
        doNothing().when(movie).setId(any());
        doNothing().when(movie).setName(any());
        doNothing().when(movie).setPreviewObjectKey(any());
        doNothing().when(movie).setPreviewUrl(any());
        movie.setDescription("The characteristics of someone or something");
        movie.setId(1);
        movie.setName("Name");
        movie.setPreviewObjectKey("Preview Object Key");
        movie.setPreviewUrl("https://example.org/example");
        Optional.of(movie);
        AdminMoviePreviewS3Service adminMoviePreviewS3Service = mock(AdminMoviePreviewS3Service.class);
        doNothing().when(adminMoviePreviewS3Service)
                .uploadMoviePreview(any(), any());
        AdminMoviePreviewS3Controller adminMoviePreviewS3Controller = new AdminMoviePreviewS3Controller(
                adminMoviePreviewS3Service);
        adminMoviePreviewS3Controller.uploadMoviePreview(1, new MockMultipartFile("Name", "AAAAAAAA".getBytes(StandardCharsets.UTF_8)));
        verify(movie).setDescription(any());
        verify(movie).setId(any());
        verify(movie).setName(any());
        verify(movie).setPreviewObjectKey(any());
        verify(movie).setPreviewUrl(any());
        verify(adminMoviePreviewS3Service).uploadMoviePreview(any(),
                any());
    }
}

